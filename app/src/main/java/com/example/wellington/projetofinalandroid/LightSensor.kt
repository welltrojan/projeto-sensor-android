package com.example.wellington.projetofinalandroid

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.TextView

class LightSensor : AppCompatActivity(), SensorEventListener {

    // Sensor
    var sensor: Sensor? = null
    // Manager
    var sensorManager: SensorManager? = null

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {

        // Variavel de status do audio
        var isRunning = false
        var textview: TextView = findViewById(R.id.sensorValue)

        if(event!!.values[0] < 40){
            val mp = MediaPlayer.create(this, R.raw.beto)
            mp.start()
            textview.text = event!!.values[0].toString()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.light_sensor)

        // Pegando Sensor
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        // Iniciando Sensor
        sensor = sensorManager!!.getDefaultSensor(Sensor.TYPE_LIGHT)

    }

    override fun onResume() {
        super.onResume()
        // Capta sensor
        sensorManager!!.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        // Parando Sensor
        sensorManager!!.unregisterListener(this )
    }


}
